(*
  Author:   Benedikt Seidl
  License:  GPLv3
*)

section \<open>Implementation of required functions for Rabin to HOA conversion\<close>

theory Rabin_to_HOA_Impl
  imports Main Rabin_to_HOA Finite_Set
begin


subsection \<open>Transition marks\<close>

--\<open>Mark assigned to a transition set\<close>
definition mark :: "('q, 'a set) transition set \<Rightarrow> mark" where
  "mark \<equiv> undefined"


subsection \<open>Labels\<close>

definition ap_to_label :: "'a set \<Rightarrow> 'a \<Rightarrow> 'a label" where
  "ap_to_label aps a \<equiv> if a \<in> aps then AP\<^sub>L a else Not\<^sub>L (AP\<^sub>L a)"

--\<open>Label for a set of propositions\<close>
definition label :: "'a list \<Rightarrow> 'a set \<Rightarrow> 'a label" where
  "label \<Sigma> aps \<equiv> fold (And\<^sub>L o ap_to_label aps) \<Sigma> True\<^sub>L"


subsection \<open>Acceptance\<close>

definition rabin_pair_to_hoa :: "('q, 'a set) rabin_pair \<Rightarrow> ('q, 'a set) hoa_acceptance" where
  "rabin_pair_to_hoa P \<equiv> And\<^sub>A (Fin\<^sub>A (fst P)) (Inf\<^sub>A (snd P))"

--\<open>Acceptance expression based on rabin condition\<close>
definition acceptance :: "('q, 'a set) rabin_condition \<Rightarrow> ('q, 'a set) hoa_acceptance" where
  "acceptance \<alpha> \<equiv> Finite_Set.fold (Or\<^sub>A o rabin_pair_to_hoa) Never\<^sub>A \<alpha>"


subsection \<open>Correctness\<close>

lemma "rabin_to_hoa_conversion mark label acceptance"
proof
  show "inj mark"
    sorry
next
  fix \<Sigma> a1 a2
  show "eval (label \<Sigma> a1) a2 = (a1 \<inter> set \<Sigma> = a2 \<inter> set \<Sigma>)"
    sorry
next
  fix \<alpha> ts
  show "accepting (acceptance \<alpha>) (Pow ts) = rabin_condition \<alpha> ts"
    sorry
qed


end