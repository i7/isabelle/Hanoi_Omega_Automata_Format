(*
  Author:   Benedikt Seidl
  License:  GPLv3
*)

section \<open>Export of HOA representation to Standard ML code\<close>

theory HOA_Automaton_Export
  imports Main HOA_Automaton HOA_Automaton_Example "HOL-Library.Code_Target_Nat" "HOL-Library.Code_Char"
begin

  datatype 'a automaton = Automaton "'a hoa_automaton"

  definition "aut \<equiv> Automaton A"

  export_code integer_of_nat Suc set True\<^sub>L Always\<^sub>A max_mark aut Automaton
    in SML module_name HOA_Automaton file "generated/HOA_Automaton.sml"

end