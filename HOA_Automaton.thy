(*
  Author:   Benedikt Seidl
  License:  GPLv3
*)

section \<open>Serialization of \<omega>-automata to the Hanoi Omega Automata Format (HOAF)\<close>

theory HOA_Automaton
  imports Main "HOL-Library.Omega_Words_Fun" "List-Index.List_Index"
begin

text \<open>
  This theory can convert several \<omega>-automata implementations to a representation
  that can be easily serialized using the Hanoi Omega Automata Format (\cite{TODO}).

  https://www.lrde.epita.fr/~adl/dl/adl/babiak.15.cav.pdf
\<close>

subsection \<open>Basic data type definitions\<close>

datatype 'a label =
  True\<^sub>L | False\<^sub>L | AP\<^sub>L 'a | Not\<^sub>L "'a label"
| And\<^sub>L "'a label" "'a label" | Or\<^sub>L "'a label"  "'a label"

datatype 'a acceptance =
  Always\<^sub>A | Never\<^sub>A | Fin\<^sub>A 'a | FinC\<^sub>A 'a | Inf\<^sub>A 'a | InfC\<^sub>A 'a
| And\<^sub>A "'a acceptance" "'a acceptance" | Or\<^sub>A "'a acceptance" "'a acceptance"

type_synonym ('q, 'a, 'm) transition\<^sub>H = "'a label \<times> 'q \<times> 'm set"
type_synonym ('q, 'a, 'm) hoa_automaton =
  "'a list \<times> ('q, 'a, 'm) transition\<^sub>H set list \<times> 'q set \<times> 'm acceptance"

abbreviation aps\<^sub>A :: "('q, 'a, 'm) hoa_automaton \<Rightarrow> 'a list" where
  "aps\<^sub>A \<equiv> fst"
abbreviation trans\<^sub>A :: "('q, 'a, 'm) hoa_automaton \<Rightarrow> ('q, 'a, 'm) transition\<^sub>H set list" where
  "trans\<^sub>A \<equiv> fst o snd"
abbreviation init\<^sub>A :: "('q, 'a, 'm) hoa_automaton \<Rightarrow> 'q set" where
  "init\<^sub>A \<equiv> fst o snd o snd"
abbreviation acc\<^sub>A :: "('q, 'a, 'm) hoa_automaton \<Rightarrow> 'm acceptance" where
  "acc\<^sub>A \<equiv> snd o snd o snd"

--\<open>Returns the largest atomic proposition found in the given label\<close>
fun max_ap :: "nat label \<Rightarrow> nat" where
  "max_ap True\<^sub>L = 0"
| "max_ap False\<^sub>L = 0"
| "max_ap (AP\<^sub>L ap) = ap"
| "max_ap (Not\<^sub>L l) = max_ap l"
| "max_ap (And\<^sub>L l1 l2) = max (max_ap l1) (max_ap l2)"
| "max_ap (Or\<^sub>L l1 l2) = max (max_ap l1) (max_ap l2)"

--\<open>Returns the largest accepting set found in the given acceptance condition\<close>
fun max_mark :: "nat acceptance \<Rightarrow> nat" where
  "max_mark Always\<^sub>A = 0"
| "max_mark Never\<^sub>A = 0"
| "max_mark (Fin\<^sub>A a) = a"
| "max_mark (FinC\<^sub>A a) = a"
| "max_mark (Inf\<^sub>A a) = a"
| "max_mark (InfC\<^sub>A a) = a"
| "max_mark (And\<^sub>A a1 a2) = max (max_mark a1) (max_mark a2)"
| "max_mark (Or\<^sub>A a1 a2) = max (max_mark a1) (max_mark a2)"

locale hoa_automaton_def =
  fixes
    --\<open>Automaton object\<close>
    A :: "('q, 'a, 'm) hoa_automaton"
  fixes
    --\<open>List of atomic propositions\<close>
    \<Sigma> :: "'a list"
  fixes
    --\<open>Transitions grouped by originating state\<close>
    \<delta> :: "('q, 'a, 'm) transition\<^sub>H set list"
  fixes
    --\<open>List of initial states\<close>
    Q\<^sub>0 :: "'q set"
  fixes
    --\<open>Acceptance condition based on accepting sets\<close>
    \<alpha> :: "'m acceptance"
  defines
    "\<Sigma> \<equiv> aps\<^sub>A A"
  defines
    "\<delta> \<equiv> trans\<^sub>A A"
  defines
    "Q\<^sub>0 \<equiv> init\<^sub>A A"
  defines
    "\<alpha> \<equiv> acc\<^sub>A A"
begin

end

subsection \<open>Semantics of \<omega>-automata\<close>

type_synonym ('q, 'a, 'm) transrel = "'q \<times> 'a set \<times> 'q \<times> 'm set"
type_synonym ('q, 'a, 'm) run\<^sub>H = "('q, 'a, 'm) transrel word"

abbreviation src\<^sub>t :: "('q, 'a, 'm) transrel \<Rightarrow> 'q" where
  "src\<^sub>t \<equiv> fst"
abbreviation aps\<^sub>t :: "('q, 'a, 'm) transrel \<Rightarrow> 'a set" where
  "aps\<^sub>t \<equiv> fst o snd"
abbreviation dst\<^sub>t :: "('q, 'a, 'm) transrel \<Rightarrow> 'q" where
  "dst\<^sub>t \<equiv> fst o snd o snd"
abbreviation marks\<^sub>t :: "('q, 'a, 'm) transrel \<Rightarrow> 'm set" where
  "marks\<^sub>t \<equiv> snd o snd o snd"

definition inf_marks :: "('q, 'a, 'm) run\<^sub>H \<Rightarrow> 'm set" where
  "inf_marks r \<equiv> \<Union> limit (marks\<^sub>t o r)"

fun eval :: "'a label \<Rightarrow> 'a set \<Rightarrow> bool" where
  "eval True\<^sub>L _ = True"
| "eval False\<^sub>L _ = False"
| "eval (AP\<^sub>L a) aps = (a \<in> aps)"
| "eval (Not\<^sub>L l) aps = (\<not>eval l aps)"
| "eval (And\<^sub>L l1 l2) aps = (eval l1 aps \<and> eval l2 aps)"
| "eval (Or\<^sub>L l1 l2) aps = (eval l1 aps \<or> eval l2 aps)"

fun accepting :: "'a acceptance \<Rightarrow> 'a set \<Rightarrow> bool" where
  "accepting Always\<^sub>A _ \<longleftrightarrow> True"
| "accepting Never\<^sub>A _ \<longleftrightarrow> False"
| "accepting (Fin\<^sub>A a) as \<longleftrightarrow> a \<notin> as"
| "accepting (FinC\<^sub>A a) as \<longleftrightarrow> a \<in> as"
| "accepting (Inf\<^sub>A a) as \<longleftrightarrow> a \<in> as"
| "accepting (InfC\<^sub>A a) as \<longleftrightarrow> a \<notin> as"
| "accepting (And\<^sub>A a1 a2) as \<longleftrightarrow> accepting a1 as \<and> accepting a2 as"
| "accepting (Or\<^sub>A a1 a2) as \<longleftrightarrow> accepting a1 as \<or> accepting a2 as"

locale hoa_automaton =
  --\<open>Extend locale and fix types\<close>
  hoa_automaton_def A for A :: "(nat, nat, nat) hoa_automaton" +
  fixes
    --\<open>Omega word\<close>
    w :: "nat set word"
  assumes
    --\<open>No undefined atomic propositions may be used\<close>
    max_ap: "\<forall>t \<in> set \<delta>. \<forall>(label, _, _) \<in> t. max_ap label < length \<Sigma>"
  assumes
    --\<open>Initial states have to be in range\<close>
    max_state_start: "\<forall>q \<in> Q\<^sub>0. q < length \<delta>"
  assumes
    --\<open>Transition destination states have to be in range\<close>
    max_state_trans: "\<forall>t \<in> set \<delta>. \<forall>(_, state, _) \<in> t. state < length \<delta>"
begin

  --\<open>Transition relation\<close>
  definition \<Delta> :: "(nat, nat, nat) transrel set" where
    "\<Delta> \<equiv> {(q, \<nu>, p, a). \<exists>\<mu>. (\<mu>, p, a) \<in> (nth \<delta> q) \<and> eval \<mu> \<nu>}"

  --\<open>Word mapping\<close>
  definition w' :: "nat set word" where
    "w' i \<equiv> index \<Sigma> ` (w i)"

  --\<open>Definition of a run\<close>
  definition is_run :: "(nat, nat, nat) run\<^sub>H \<Rightarrow> bool" where
    "is_run r \<equiv> (\<forall>i. r i \<in> \<Delta>)
      \<and> src\<^sub>t (r 0) \<in> Q\<^sub>0
      \<and> (\<forall>i. dst\<^sub>t (r i) = src\<^sub>t (r (i + 1)))
      \<and> (\<forall>i. aps\<^sub>t (r i) = w' i)"

  --\<open>Acceptance property of the omega word\<close>
  definition accept :: bool where
    "accept \<equiv> \<exists>r. is_run r \<and> accepting \<alpha> (inf_marks r)"

end

declare
  hoa_automaton.\<Delta>_def
  hoa_automaton.w'_def
  hoa_automaton.is_run_def
  hoa_automaton.accept_def
[simp]

definition "accept\<^sub>H \<equiv> hoa_automaton.accept"

end