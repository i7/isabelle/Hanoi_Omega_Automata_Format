structure HOA_Automaton : sig
  type nat
  val integer_of_nat : nat -> IntInf.int
  datatype 'a set = Set of 'a list | Coset of 'a list
  datatype label = True_L | False_L | AP_L of nat | Not_L of label |
    And_L of label * label | Or_L of label * label
  datatype acceptance = Always_A | Never_A | Fin_A of nat | FinC_A of nat |
    Inf_A of nat | InfC_A of nat | And_A of acceptance * acceptance |
    Or_A of acceptance * acceptance
  datatype 'a automaton =
    Automaton of
      ('a list * ((label * (nat * nat set)) set list * (nat set * acceptance)))
  val suc : nat -> nat
  val max_mark : acceptance -> nat
  val aut : (char list) automaton
end = struct

datatype nat = Nat of IntInf.int;

fun integer_of_nat (Nat x) = x;

fun equal_nata m n = (((integer_of_nat m) : IntInf.int) = (integer_of_nat n));

type 'a equal = {equal : 'a -> 'a -> bool};
val equal = #equal : 'a equal -> 'a -> 'a -> bool;

val equal_nat = {equal = equal_nata} : nat equal;

datatype num = One | Bit0 of num | Bit1 of num;

val one_nata : nat = Nat (1 : IntInf.int);

type 'a one = {one : 'a};
val one = #one : 'a one -> 'a;

val one_nat = {one = one_nata} : nat one;

val zero_nata : nat = Nat (0 : IntInf.int);

type 'a zero = {zero : 'a};
val zero = #zero : 'a zero -> 'a;

val zero_nat = {zero = zero_nata} : nat zero;

fun less_eq_nat m n = IntInf.<= (integer_of_nat m, integer_of_nat n);

type 'a ord = {less_eq : 'a -> 'a -> bool, less : 'a -> 'a -> bool};
val less_eq = #less_eq : 'a ord -> 'a -> 'a -> bool;
val less = #less : 'a ord -> 'a -> 'a -> bool;

fun less_nat m n = IntInf.< (integer_of_nat m, integer_of_nat n);

val ord_nat = {less_eq = less_eq_nat, less = less_nat} : nat ord;

fun list_all p [] = true
  | list_all p (x :: xs) = p x andalso list_all p xs;

datatype 'a set = Set of 'a list | Coset of 'a list;

fun eq A_ a b = equal A_ a b;

fun membera A_ [] y = false
  | membera A_ (x :: xs) y = eq A_ x y orelse membera A_ xs y;

fun member A_ x (Coset xs) = not (membera A_ xs x)
  | member A_ x (Set xs) = membera A_ xs x;

fun less_eq_set A_ (Coset []) (Set []) = false
  | less_eq_set A_ a (Coset ys) = list_all (fn y => not (member A_ y a)) ys
  | less_eq_set A_ (Set xs) b = list_all (fn x => member A_ x b) xs;

fun equal_seta A_ a b = less_eq_set A_ a b andalso less_eq_set A_ b a;

fun equal_set A_ = {equal = equal_seta A_} : 'a set equal;

fun equal_proda A_ B_ (x1, x2) (y1, y2) = eq A_ x1 y1 andalso eq B_ x2 y2;

fun equal_prod A_ B_ = {equal = equal_proda A_ B_} : ('a * 'b) equal;

datatype label = True_L | False_L | AP_L of nat | Not_L of label |
  And_L of label * label | Or_L of label * label;

fun equal_labela (And_L (x51, x52)) (Or_L (x61, x62)) = false
  | equal_labela (Or_L (x61, x62)) (And_L (x51, x52)) = false
  | equal_labela (Not_L x4) (Or_L (x61, x62)) = false
  | equal_labela (Or_L (x61, x62)) (Not_L x4) = false
  | equal_labela (Not_L x4) (And_L (x51, x52)) = false
  | equal_labela (And_L (x51, x52)) (Not_L x4) = false
  | equal_labela (AP_L x3) (Or_L (x61, x62)) = false
  | equal_labela (Or_L (x61, x62)) (AP_L x3) = false
  | equal_labela (AP_L x3) (And_L (x51, x52)) = false
  | equal_labela (And_L (x51, x52)) (AP_L x3) = false
  | equal_labela (AP_L x3) (Not_L x4) = false
  | equal_labela (Not_L x4) (AP_L x3) = false
  | equal_labela False_L (Or_L (x61, x62)) = false
  | equal_labela (Or_L (x61, x62)) False_L = false
  | equal_labela False_L (And_L (x51, x52)) = false
  | equal_labela (And_L (x51, x52)) False_L = false
  | equal_labela False_L (Not_L x4) = false
  | equal_labela (Not_L x4) False_L = false
  | equal_labela False_L (AP_L x3) = false
  | equal_labela (AP_L x3) False_L = false
  | equal_labela True_L (Or_L (x61, x62)) = false
  | equal_labela (Or_L (x61, x62)) True_L = false
  | equal_labela True_L (And_L (x51, x52)) = false
  | equal_labela (And_L (x51, x52)) True_L = false
  | equal_labela True_L (Not_L x4) = false
  | equal_labela (Not_L x4) True_L = false
  | equal_labela True_L (AP_L x3) = false
  | equal_labela (AP_L x3) True_L = false
  | equal_labela True_L False_L = false
  | equal_labela False_L True_L = false
  | equal_labela (Or_L (x61, x62)) (Or_L (y61, y62)) =
    equal_labela x61 y61 andalso equal_labela x62 y62
  | equal_labela (And_L (x51, x52)) (And_L (y51, y52)) =
    equal_labela x51 y51 andalso equal_labela x52 y52
  | equal_labela (Not_L x4) (Not_L y4) = equal_labela x4 y4
  | equal_labela (AP_L x3) (AP_L y3) = equal_nata x3 y3
  | equal_labela False_L False_L = true
  | equal_labela True_L True_L = true;

val equal_label = {equal = equal_labela} : label equal;

datatype acceptance = Always_A | Never_A | Fin_A of nat | FinC_A of nat |
  Inf_A of nat | InfC_A of nat | And_A of acceptance * acceptance |
  Or_A of acceptance * acceptance;

datatype 'a automaton =
  Automaton of
    ('a list * ((label * (nat * nat set)) set list * (nat set * acceptance)));

fun plus_nat m n = Nat (IntInf.+ (integer_of_nat m, integer_of_nat n));

fun suc n = plus_nat n one_nata;

fun removeAll A_ x [] = []
  | removeAll A_ x (y :: xs) =
    (if eq A_ x y then removeAll A_ x xs else y :: removeAll A_ x xs);

fun inserta A_ x xs = (if membera A_ xs x then xs else x :: xs);

fun insert A_ x (Coset xs) = Coset (removeAll A_ x xs)
  | insert A_ x (Set xs) = Set (inserta A_ x xs);

fun max A_ a b = (if less_eq A_ a b then b else a);

fun max_mark Always_A = zero_nata
  | max_mark Never_A = zero_nata
  | max_mark (Fin_A a) = a
  | max_mark (FinC_A a) = a
  | max_mark (Inf_A a) = a
  | max_mark (InfC_A a) = a
  | max_mark (And_A (a1, a2)) = max ord_nat (max_mark a1) (max_mark a2)
  | max_mark (Or_A (a1, a2)) = max ord_nat (max_mark a1) (max_mark a2);

val bot_set : 'a set = Set [];

fun q_0 (A1_, A2_) = insert A2_ (zero A1_) bot_set;

fun delta (A1_, A2_, A3_) (B1_, B2_) =
  [insert (equal_prod equal_label (equal_prod A3_ (equal_set B2_)))
     (AP_L zero_nata, (zero A2_, bot_set))
     (insert (equal_prod equal_label (equal_prod A3_ (equal_set B2_)))
       (AP_L one_nata, (zero A2_, bot_set))
       (insert (equal_prod equal_label (equal_prod A3_ (equal_set B2_)))
         (AP_L zero_nata, (one A1_, bot_set)) bot_set)),
    insert (equal_prod equal_label (equal_prod A3_ (equal_set B2_)))
      (AP_L zero_nata, (one A1_, insert B2_ (zero B1_) bot_set)) bot_set];

val alpha : acceptance = Inf_A zero_nata;

val sigma : (char list) list = [[#"0"], [#"1"]];

val a :
  (char list) list *
    ((label * (nat * nat set)) set list * (nat set * acceptance))
  = (sigma,
      (delta (one_nat, zero_nat, equal_nat) (zero_nat, equal_nat),
        (q_0 (zero_nat, equal_nat), alpha)));

val aut : (char list) automaton = Automaton a;

end; (*struct HOA_Automaton*)
