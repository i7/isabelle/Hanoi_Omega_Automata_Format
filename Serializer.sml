use "generated/HOA_Automaton.sml";

open HOA_Automaton;

structure Serializer : sig
  val serialize : char list automaton -> string
end = struct

(*** Tool meta data ***)

val tool = "Isabelle HOA serializer"
val version = "0.1"
val properties = ["trans-labels", "explicit-labels", "trans-acc", "no-univ-branch"]

(*** Helper functions ***)

fun mapi f xs =
  let
    fun inner _ acc [] = List.rev acc
      | inner i acc (x :: xs) = inner (i + 1) (f i x :: acc) xs
  in
    inner 0 [] xs
  end

fun nat_to_string n = IntInf.toString (integer_of_nat n)

fun set_to_list (Set l) = l
  | set_to_list (Coset _) = raise Fail "Cosets are not supported"

fun set_map f s = List.map f (set_to_list s)

fun escape_char #"\"" = "\\\""
  | escape_char #"\\" = "\\\\"
  | escape_char c = Char.toString c

fun quote_string str = " \"" ^ String.translate escape_char str ^ "\""

(*** Serialize nested data structures ***)

fun serialize_acc Always_A = "t"
  | serialize_acc Never_A  = "f"
  | serialize_acc (Fin_A n) = "Fin(" ^ nat_to_string n ^ ")"
  | serialize_acc (FinC_A n) = "Fin(!" ^ nat_to_string n ^ ")"
  | serialize_acc (Inf_A n) = "Inf(" ^ nat_to_string n ^ ")"
  | serialize_acc (InfC_A n) = "Inf(!" ^ nat_to_string n ^ ")"
  | serialize_acc (And_A  (a1, a2)) =
      "(" ^ serialize_acc a1 ^ "&" ^ serialize_acc a2 ^ ")"
  | serialize_acc (Or_A (a1, a2)) =
      "(" ^ serialize_acc a1 ^ "|" ^ serialize_acc a2 ^ ")"

fun serialize_label True_L = "t"
  | serialize_label False_L = "f"
  | serialize_label (AP_L i) = nat_to_string i
  | serialize_label (Not_L l) = "(!" ^ serialize_label l ^ ")"
  | serialize_label (And_L (l1, l2)) =
      "(" ^ serialize_label l1 ^ "&" ^ serialize_label l2 ^ ")"
  | serialize_label (Or_L (l1, l2)) =
      "(" ^ serialize_label l1 ^ "|" ^ serialize_label l2 ^ ")"

(*** Serialize header ***)

fun serialize_start state =
  "Start: " ^ nat_to_string state ^ "\n"

fun serialize_aps aps =
  Int.toString (List.length aps)
    ^ String.concat (List.map (quote_string o String.implode) aps)

fun serialize_max_acc acc = nat_to_string (suc (max_mark acc))

fun serialize_header aps trans start acc =
  "HOA: v1\n"
    ^ "tool:" ^ quote_string tool ^ quote_string version ^ "\n"
    ^ "properties: " ^ String.concatWith " " properties ^ "\n"
    ^ "States: " ^ Int.toString (List.length trans) ^ "\n"
    ^ String.concat (set_map serialize_start start)
    ^ "AP: " ^ serialize_aps aps ^ "\n"
    ^ "Acceptance: " ^ serialize_max_acc acc ^ " " ^ serialize_acc acc ^ "\n"

(*** Serialize body ***)

fun serialize_acc_set [] = ""
  | serialize_acc_set acc_set =
    " {" ^ String.concatWith " " (List.map nat_to_string acc_set) ^ "}"

fun serialize_transition (label, (destination, acc_set)) =
  "[" ^ serialize_label label ^ "]" ^ " "
    ^ nat_to_string destination
    ^ serialize_acc_set (set_to_list acc_set) ^ "\n"

fun serialize_state state transitions =
  "State: " ^ Int.toString state ^ "\n"
    ^ String.concat (set_map serialize_transition transitions)

(*** Serialize automaton ***)

fun serialize (Automaton (aps, (trans, (start, acc)))) =
  serialize_header aps trans start acc
    ^ "--BODY--\n"
    ^ String.concat (mapi serialize_state trans)
    ^ "--END--\n"

end; (*struct Serializer*)
    